import React from 'react';
import { render } from '@testing-library/react';
import Button from '../../Components/Button';

test("renders button with text", () => {
  const { getByText } = render(
    <Button
      tooltip={""}
      text={"button with some text"}
    />
  );
  const linkElement = getByText(/button with some text/i);
  expect(linkElement).toBeInTheDocument();
});
