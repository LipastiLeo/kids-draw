import React from 'react';
import { render } from '@testing-library/react';
import Checkbox from '../../Components/Checkbox';

test("renders checkbox with text", () => {
  const { getByText } = render(
    <Checkbox
      tooltip={""}
      text={"checkbox with some text"}
    />
  );
  const linkElement = getByText(/checkbox with some text/i);
  expect(linkElement).toBeInTheDocument();
});
