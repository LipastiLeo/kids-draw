import React from 'react';
import { render } from '@testing-library/react';
import Input from '../../Components/Input';

test("renders input with text", () => {
  const { getByText } = render(
    <Input
      tooltip={""}
      text={"input with some text"}
      type={"text"}
      placeholder={""}
    />
  );
  const linkElement = getByText(/input with some text/i);
  expect(linkElement).toBeInTheDocument();
});
