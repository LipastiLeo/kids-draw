// For this simple app redux would be overkill
// we use localstorage to get and set app data

type appOption = {
  rememberMe: string, 
} 

export function localStore(options: appOption) {
  if (!!options.rememberMe) {
    localStorage.setItem('rememberMe',options.rememberMe)
  }
  return null;
}

export function localRestore() {
  const options = { 
    rememberMe: localStorage.getItem('rememberMe') === 'true' ? true : false
  };
  return options;
}

export function localReset() {
  localStorage.removeItem('rememberMe');
  return null;
}