
export function sessionStore(token: string) {
  sessionStorage.setItem('session',token)
  return true;
}

export function sessionRestore() {
  const token = sessionStorage.getItem('session');
  return token;
}

export function sessionReset() {
  sessionStorage.removeItem('session')
  return null;
}