const HttpStatus = require('http-status-codes');
const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');

const app = express();

app.set('port', process.env.PORT || 8080);

// Implement health/status call
app.use('/health', (req, res, next) => {
  res.header('Content-Type', 'text/plain; charset=utf-8');
  res.status(HttpStatus.OK).send();
});

// Serve static content from the build folder
app.use(express.static('build'));

// Proxy calls using /server to the server
const serverProxy = createProxyMiddleware({
  target: process.env.SERVER || 'http://localhost:3000',
  logLevel: 'debug',
  pathRewrite: function (path, req) { return path.replace('/server', '') },
  changeOrigin: true,
});

app.use('/server', serverProxy);

// Not found error handling, redirect to /
/* eslint-disable no-unused-vars */
app.use((req, res) => {
  res.redirect('/')
});

// 500 error handling
app.use((err, req, res, next) => {
  console.log(`Internal server error: ${err}`);
  res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(JSON.stringify({
    status: 'error',
    message: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR),
  }));
});
/* eslint-enable no-unused-vars */

// Express error logging
app.on('error', (err) => {
  console.log(`Express: ${err}`);
});

// Start web server using defined port
app.listen(app.get('port'), () => {
  console.log(`Kids-Draw is running on port ${app.get('port')}`);
});
