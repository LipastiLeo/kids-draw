import React from 'react';
import Form from 'react-bootstrap/Form';
import Tooltip from 'react-bootstrap/Tooltip';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';

type MyProps = {
  tooltip: string;
  text: string;
  type: string;
  placeholder: string;
  changeHandler: any;
};
class Inputs extends React.Component<MyProps> {
  render() {
    return (
      this.props.tooltip === "" ? (
        <Form.Group controlId={`${this.props.text.toLocaleLowerCase()}`} aria-label={this.props.text}>
          <Form.Label>{this.props.text}</Form.Label>
          <Form.Control type={this.props.type} placeholder={this.props.placeholder} />
        </Form.Group>
      ) : (
        <OverlayTrigger
          key={this.props.tooltip}
          placement={'bottom'}
          overlay={
            <Tooltip id={`tooltip-${this.props.tooltip}`}>
              {this.props.tooltip}
            </Tooltip>
          }
        >
          <Form.Group controlId={`${this.props.text.toLocaleLowerCase()}`} aria-label={this.props.text}>
            <Form.Label>{this.props.text}</Form.Label>
            <Form.Control type={this.props.type} name={this.props.text.toLocaleLowerCase()} onChange={this.props.changeHandler.bind(this)} placeholder={this.props.placeholder} />
          </Form.Group>
        </OverlayTrigger>
      )
    );
  }
}
export default Inputs;