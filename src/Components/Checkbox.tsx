import React from 'react';
import Form from 'react-bootstrap/Form';
import Tooltip from 'react-bootstrap/Tooltip';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';

type MyProps = {
  tooltip: string;
  text: string;
};
class Checkbox extends React.Component<MyProps> {
  render() {
    return (
      this.props.tooltip === "" ? (
        <Form.Group controlId={`formBasicCheckbox`}>
          <Form.Check type="checkbox" label={this.props.text} />
        </Form.Group>
      ) : (
        <OverlayTrigger
          key={this.props.tooltip}
          placement={'bottom'}
          overlay={
            <Tooltip id={`tooltip-${this.props.tooltip}`}>
              {this.props.tooltip}
            </Tooltip>
          }
        >
          <Form.Group controlId={`formBasicCheckbox`}>
            <Form.Check type="checkbox" label={this.props.text} />
          </Form.Group>
        </OverlayTrigger>
      )
    );
  }
}
export default Checkbox;