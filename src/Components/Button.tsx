import React from 'react';
import Button from 'react-bootstrap/Button';
import Tooltip from 'react-bootstrap/Tooltip';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';

type MyProps = {
  tooltip: string;
  text: string;
};
class Buttons extends React.Component<MyProps> {
  render() {
    return (
      this.props.tooltip === "" ? (
        <Button aria-label={this.props.text}>
          {this.props.text}
        </Button>
      ) : (
        <OverlayTrigger
          key={this.props.tooltip}
          placement={'bottom'}
          overlay={
            <Tooltip id={`tooltip-${this.props.tooltip}`}>
              {this.props.tooltip}
            </Tooltip>
          }
        >
          <Button>
            {this.props.text}
          </Button>
        </OverlayTrigger>
      )
    );
  }
}
export default Buttons;