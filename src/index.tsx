import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import Login from './Pages/Login';
import Register from './Pages/Register';
import Dashboard from './Pages/Dashboard'
import { localRestore } from './storage/localStorage';
import { sessionRestore } from './storage/sessionStorage';

// Importing the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <App />
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

function App() {
  let options = localRestore();
  let token = sessionRestore();
  return (
    <Switch>

      <Route path={'/register'}>
        <Register />
      </Route>

      <Route path={'/'}>
        {!!token ? <Redirect to={'dashboard'} /> : <Login />}
      </Route>

      <Route path={'/login'}>
        {options.rememberMe ? <Redirect to={'dashboard'} /> : <Login />}
      </Route>

      <Route path={"/dashboard"}>
        {!options.rememberMe ? <Redirect to={'login'} /> : <Dashboard />}
      </Route>

    </Switch>
  );
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
