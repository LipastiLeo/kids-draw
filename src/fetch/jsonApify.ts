export function jsonApifyBody(body: Object) {
  // This is oversimplification
  // but we will "guess" data type
  
  let bodytype = "undefined";
  // Login and Register type
  if (body.hasOwnProperty("password")) {
    bodytype = "users";
  }

  const returnObject = {
    data: {
      type: bodytype,
      attributes: body
    }
  };

  return returnObject;
}