import fetch from 'isomorphic-unfetch';
import { sessionStore } from '../storage/sessionStorage';
import { sessionRestore } from '../storage/sessionStorage';
import { sessionReset } from '../storage/sessionStorage';
import { localRestore } from '../storage/localStorage';
import { jsonApifyBody } from './jsonApify';

let sessionMemoryToken = '';

type option = {
  method: string, 
  mode?: 'cors', 
  body?: object,
  headers?: object
} 
export default async function proxyFetch(
    path: string,
    options: option,
    body?: object,
    headers?: object
  ) {
    // default body
    const defaultBody = {
      jsonapi: {
        version: "0.1.0",
        meta: {
          author: "Leo Lipasti",
          description: "Kids-Draw"
        }
      }
    };
    // assign body
    const assignBody = Object.assign(defaultBody, jsonApifyBody(body ? body : {}));

    // default headers
    const defaultHeaders = {
      'Content-Type': 'application/vnd.api+json; charset=utf-8',
      'Authorization': ''
    }

    // assign headers
    const assignHeaders = Object.assign(defaultHeaders, headers);

    // set auth header
    const token = sessionRestore();
    if (!!token) {
      assignHeaders.Authorization = `${token}`;
    } else if (sessionMemoryToken !== '') {
      assignHeaders.Authorization = `${sessionMemoryToken}`;
    }

    const appOptions = localRestore();

    // any fetch call! All requests!
    return fetch(path, {
      method: options.method,
      credentials: 'include',
      mode: options.mode,
      body: options.method !== 'GET' ? JSON.stringify(assignBody) : undefined,
      headers: assignHeaders,
    })
    .then((response) => {
      // token was ok and data received
      if (response.status === 200) {
        return response.json();
      }
      // 409: token expired. Make a refresh token request
      else if (response.status === 409) {
        fetch('/server/token', {
          method: 'GET',
          credentials: 'include',
          mode: options.mode,
          body: undefined,
          headers: assignHeaders,
        })
        .then(response => response.json())
        .then((response) => {
          // token received. Store
          if (!!response.accessToken) {
            // remember me sessionStore :
            if (appOptions.rememberMe) {
              sessionStore(response.accessToken);
            } else {
              // normal memory store :
              sessionMemoryToken = response.accessToken;
            }
            // set auth header
            assignHeaders.Authorization = `${response.accessToken}`;
          } else {
            // failure to authenticate, reset stored token. No more requests made
            sessionReset();
            return response;
          }

          // we have a new token by this point so we can make the request again
          return fetch(path, {
            method: options.method,
            credentials: 'include',
            mode: options.mode,
            body: options.method !== 'GET' ? JSON.stringify(assignBody) : undefined,
            headers: assignHeaders,
          })
          .then(response => response.json())
          .then((response) => {
            return response;
          })
        })
      } else {
        return response;
      }
    })
    .then((response) => {
      // token received? Store
      if (!!response.accessToken) {
        // remember me sessionStore :
        if (appOptions.rememberMe) {
          sessionStore(response.accessToken);
        } else {
          // normal memory store :
          sessionMemoryToken = response.accessToken;
        }
      }
      return response;
    });
}