import React from 'react';
import Button from '../Components/Button'
import Input from '../Components/Input'
import proxyFetch from '../fetch/proxyFetch'
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import {
  Redirect,
  Link
} from 'react-router-dom';
import { localStore } from '../storage/localStorage';

type MyProps = {
};

type MyState = {
  redirect: string;
  error: string;
  toggleRememberme: boolean;
  username: string;
  email: string;
  password: string;
};

class Register extends React.Component<MyProps, MyState> {
  state: MyState = {
    redirect: '',
    error: '',
    toggleRememberme: false,
    username: '',
    email: '',
    password: '',
  };
  constructor(props: any) {
    super(props);
    // This binding is necessary to make `this` work in the callback
    this.submitHandler = this.submitHandler.bind(this);
    this.rememberUserHandler = this.rememberUserHandler.bind(this);
  }

  changeHandler = (event: any) => {
    this.setState({...this.state, [event.target.id]: event.target.value});
  }
  
  submitHandler() {
    proxyFetch('/server/users', {method: 'POST'}, {
      username: this.state.username,
      email: this.state.email,
      password: this.state.password
    })
    .then(result => {
      console.log(result);
      if (result.accessToken) {
        this.setState({ redirect: "/dashboard", error: "" });
      } else {
        // bad login attempt
        if (result.status === 401) {
          this.setState({ error: "Bad username or password. Try again." });
        }
        else if (result.status === 504) {
          this.setState({ error: "Server is busy or offline. Please try again later." });
        }
      }
    });
  }

  rememberUserHandler() {
    this.setState({ toggleRememberme: !this.state.toggleRememberme });
    localStore({ rememberMe: this.state.toggleRememberme.toString() });
  }

  render() {
    if (this.state.redirect !== '') {
      return <Redirect to={this.state.redirect} />
    }
    return (
      <div className="container">
        <div className="row h-100 mt-5">
          <div className="col-sm-8 col-md-4 mx-auto mt-5">
            <Card bg='light'>
              <Card.Body>
                <div className="d-flex justify-content-center">
                  <div className="p-2">
                    <Card.Title>Register to Kids Draw</Card.Title>
                  </div>
                </div>
                <div className="d-flex justify-content-center">
                  <div className="p-2">
                    <em>Already a user?</em> 
                    <Link to={`/login`}>
                      <a href="#" className="badge badge-light">Login</a>
                    </Link>
                  </div>
                </div>
                {this.state.error !== '' && (
                  <div className="alert alert-primary" role="alert">{this.state.error}</div>
                )}
                <Form>
                  <Input
                    tooltip={'register using your email'}
                    text={'Email'}
                    type={'email'}
                    placeholder={''}
                    changeHandler={this.changeHandler}
                  />
                  <Input
                    tooltip={'give your account a password'}
                    text={'Password'}
                    type={'password'}
                    placeholder={''}
                    changeHandler={this.changeHandler}
                  />
                  <Input
                    tooltip={'this username is shown when you send friend requests'}
                    text={'Username'}
                    type={'text'}
                    placeholder={'e.g. your real name / nickname'}
                    changeHandler={this.changeHandler}
                  />
                  <div className="d-flex justify-content-center">
                    <div className="p-2" onClick={this.submitHandler}>
                      <Button
                        tooltip={''}
                        text={'register'}
                      />
                    </div>
                  </div>
                </Form>
              </Card.Body>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
