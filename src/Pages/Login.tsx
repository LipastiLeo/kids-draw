import React from 'react';
import Button from '../Components/Button'
import Input from '../Components/Input'
import Checkbox from '../Components/Checkbox'
import proxyFetch from '../fetch/proxyFetch'
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import {
  Redirect,
  Link
} from 'react-router-dom';
import { localStore } from '../storage/localStorage';

type MyProps = {
};

type MyState = {
  redirect: string;
  error: string;
  toggleRememberme: boolean;
  email: string;
  password: string;
};

class Login extends React.Component<MyProps, MyState> {
  state: MyState = {
    redirect: '',
    error: '',
    toggleRememberme: false,
    email: '',
    password: ''
  };
  constructor(props: any) {
    super(props);
    // This binding is necessary to make `this` work in the callback
    this.submitHandler = this.submitHandler.bind(this);
    this.rememberUserHandler = this.rememberUserHandler.bind(this);
  }

  changeHandler = (event: any) => {
    this.setState({...this.state, [event.target.id]: event.target.value});
  }
  
  submitHandler() {
    proxyFetch('/server/login', {method: 'POST'}, { 
      email: this.state.email, password: this.state.password 
    })
    .then(result => {
      if (result.accessToken) {
        this.setState({ redirect: "/dashboard", error: "" });
      } else {
        // bad login attempt
        if (result.status === 401) {
          this.setState({ error: "Bad username or password. Try again." });
        }
        else if (result.status === 504) {
          this.setState({ error: "Server is busy or offline. Please try again later." });
        }
      }
    });
  }

  rememberUserHandler() {
    this.setState({ toggleRememberme: !this.state.toggleRememberme });
    localStore({ rememberMe: this.state.toggleRememberme.toString() });
  }

  render() {
    if (this.state.redirect !== '') {
      return <Redirect to={this.state.redirect} />
    }
    return (
      <div className="container">
        <div className="row h-100 mt-5">
          <div className="col-sm-8 col-md-4 mx-auto mt-5">
            <Card bg='light'>
              <Card.Body>
                <div className="d-flex justify-content-center">
                  <div className="p-2">
                    <Card.Title>Login to Kids Draw</Card.Title>
                  </div>
                </div>
                <div className="d-flex justify-content-center">
                  <div className="p-2">
                    <em>New user?</em> 
                    <Link to={`/register`}>
                      <a href="#" className="badge badge-light">Create account</a>
                    </Link>
                  </div>
                </div>
                {this.state.error !== '' && (
                  <div className="alert alert-primary" role="alert">{this.state.error}</div>
                )}
                <Form>
                  <Input
                    tooltip={'login using your master email'}
                    text={'Email'}
                    type={'email'}
                    placeholder={''}
                    changeHandler={this.changeHandler}
                  />
                  <Input
                    tooltip={'login using your master password'}
                    text={'Password'}
                    type={'password'}
                    placeholder={''}
                    changeHandler={this.changeHandler}
                  />
                  <div onClick={this.rememberUserHandler}>
                    <Checkbox
                      tooltip={'Automatically login master on this device. Recommended for good user experience. You can set separate settings for sub-accounts (family members)'}
                      text={'Remember me'}
                    />
                  </div>
                  <div className="d-flex justify-content-center">
                    <div className="p-2" onClick={this.submitHandler}>
                      <Button
                        tooltip={''}
                        text={'login'}
                      />
                    </div>
                  </div>
                </Form>
              </Card.Body>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
