## Overview

Kids Draw is a drawing app for parents and children. Connect your children to their friends manually in a secure network so they can share each other drawn images or optionally just use the app for drawing without social feature.

This is the frontend.
https://gitlab.com/LipastiLeo/kids-draw/

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:8080] to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## React helmet

This reusable React component will manage all of your changes to the document head.

Helmet takes plain HTML tags and outputs plain HTML tags.

## Bootstrap

This project uses bootstrap as components with react-bootstrap.

## Typescript

This app was created using create-react-app ... --template typescript.

## Trivial

### Intendation

Two spaces

### Commits

Commit script uses globally installed commitizen.<br />
npm install -g commitizen<br />
commitizen init cz-conventional-changelog --save-dev --save-exact

